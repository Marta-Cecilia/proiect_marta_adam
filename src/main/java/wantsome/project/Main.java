package wantsome.project;

import spark.Spark;
import wantsome.project.db.service.DbInitService;
import wantsome.project.web.*;

import static spark.Spark.*;


/**
 * Main class of the application. Using Spark framework.
 */
public class Main {

    public static void main(String[] args) {
        setup();
        configureRoutesAndStart();
    }

    private static void setup() {
        DbInitService.createMissingTables();
        DbInitService.createDefaultExpensesCategories();
    }

    private static void configureRoutesAndStart() {

        Spark.port(4648);
        staticFileLocation("public");

        get("/main", MainPageController::showMainPage);

        get("/dashboard", DashboardPageController::showDashboardPage);

        get( "/transactions", TransactionsPageController :: showTransactionsPage);

        get("/transactions/add", AddEditTransactionPageController::showAddForm);
        post("/transactions/add", AddEditTransactionPageController::handleAddUpdatePostRequest);

        get("transactions/update/:id", AddEditTransactionPageController::showUpdateForm);
        post("transactions/update/:id", AddEditTransactionPageController::handleAddUpdatePostRequest);


        get("/transactions/delete/:id", TransactionsPageController::handleDeleteRequest);



        get("/categories", CategoriesPageController::showCategoriesPage);

        get("/categories/add", AddEditCategoryPageController::showAddForm);
        post("/categories/add", AddEditCategoryPageController::handleAddUpdateRequest);


        get("/categories/update/:id", AddEditCategoryPageController::showUpdateForm);
        post("/categories/update/:id", AddEditCategoryPageController::handleAddUpdateRequest);
        get("/categories/delete/:id", CategoriesPageController::handleDeleteRequest);




        exception(Exception.class, ErrorPageController::handleException);

        awaitInitialization();
        System.out.println("\nServer started: http://localhost:4648/main");
    }


}
