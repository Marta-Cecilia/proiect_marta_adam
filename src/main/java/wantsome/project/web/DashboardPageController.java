package wantsome.project.web;

import spark.Request;
import spark.Response;
import wantsome.project.db.dto.TransactionFull;
import wantsome.project.db.dto.Type;
import wantsome.project.db.service.TransactionDao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.*;
import static wantsome.project.web.SparkUtil.render;

public class DashboardPageController {
    private static List<TransactionFull> allTransactions = TransactionDao.loadAll();


    public static String showDashboardPage(Request request, Response response) {


        Map<String, Long> categoryVsTotalMap = allTransactions.stream()
            .collect(groupingBy(TransactionFull::getCategoryDescription, counting()));

        Map<String, Object> model = new HashMap<>();
        model.put("categoryVsTotalMap", categoryVsTotalMap);
        model.put("totalExpenses", String.format("%.2f",computeTotalAmount(allTransactions, Type.EXPENSE)));
        model.put("totalIncome", String.format("%.2f",computeTotalAmount(allTransactions, Type.INCOME)));
        return render("dashboard.vm", model);



    }


   private static Double computeTotalAmount (List<TransactionFull> transactionList, Type type){
        return transactionList.stream()
                .filter(n-> n.getCategoryType()==type)
                .map(TransactionFull::getAmount)
                .mapToDouble(n-> n).sum();
   }
    }
