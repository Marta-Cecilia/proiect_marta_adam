package wantsome.project.web;

import spark.Request;
import spark.Response;
import wantsome.project.db.dto.Category;
import wantsome.project.db.dto.Type;
import wantsome.project.db.service.CategoryDao;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static wantsome.project.web.SparkUtil.render;

public class CategoriesPageController {


    public static String showCategoriesPage(Request request, Response response) {
        return renderCategoriesPage(null);
    }

    private static String renderCategoriesPage(String errorMsg) {
        Map<String, Object> model = new HashMap<>();
        model.put("categories", CategoryDao.getAll());
        model.put("errorMsg", errorMsg);
        return render("categories.vm", model);
    }

    public static Object handleChangeTypeRequest(Request request, Response response) {
        String idParam = request.params("id");
        if (!idParam.isEmpty()) {
            long id = Long.parseLong(idParam);
            Optional<Category> optionalCategory = CategoryDao.get(id);
            optionalCategory.ifPresent(category -> {
                Type newType = category.getType() == Type.EXPENSE ? Type.INCOME : Type.EXPENSE;
                Category updatedCategory = new Category(category.getId(), category.getDescription(), newType);
                CategoryDao.update(updatedCategory);
            });
        }
        response.redirect("/transactions");
        return response;
    }

    public static Object handleDeleteRequest(Request request, Response response) {
        String id = request.params("id");
        String errorMsg = null;
        try {
            CategoryDao.delete(Long.parseLong(id));
        } catch (Exception e) {
           throw new RuntimeException("Failed to delete category with id " + id + ": " + e.getMessage()) ;
        }
        return renderCategoriesPage(errorMsg);
    }
}
