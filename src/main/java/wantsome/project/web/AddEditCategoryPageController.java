package wantsome.project.web;

import spark.Request;
import spark.Response;
import wantsome.project.db.dto.Category;
import wantsome.project.db.dto.Type;
import wantsome.project.db.service.CategoryDao;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static wantsome.project.web.SparkUtil.render;

public class AddEditCategoryPageController {

    public static String showAddForm (Request request, Response response ) {
        return renderAddUpdateForm("", "", Type.INCOME.name(),null);
    }

    public static String showUpdateForm (Request request, Response response) {

        String id = request.params("id");
        Optional<Category> optCat = CategoryDao.get(Integer.parseInt(id));
        if (!optCat.isPresent()) {
            throw new RuntimeException("Category " + id + " not found!");
        }

        Category category = optCat.get();
        return renderAddUpdateForm(
                     String.valueOf(category.getId()),
                     String.valueOf(category.getDescription()),
                     category.getType().name(),
                    ""
        );

    }

    private static String renderAddUpdateForm(String id, String description, String type, String errorMsg) {
        Map<String, Object> model = new HashMap<>();
        model.put("prevId", id);
        model.put("prevDescription", description);
        model.put("prevType", type);
        model.put("errorMsg", errorMsg);
        model.put("isUpdate", id != null && !id.isEmpty());

        model.put("typeOptions",Type.values()); //DE COMPLETAT IN VM FILE

        return render("add_edit_category.vm", model);
    }


    public static Object handleAddUpdateRequest(Request request, Response response) {

        String id = request.queryParams("id");
        String description = request.queryParams("description");
        String type = request.queryParams("type");

        try {
            Category cat = validateAndBuildCategory(id, description,type);

            if (cat.getId() > 0) { //update case
                CategoryDao.update(cat);
            } else {
                CategoryDao.insert(cat);
            }

            response.redirect("/categories");
            return response;

        } catch (Exception e) {
            return renderAddUpdateForm(id, description,type,  e.getMessage());
        }
    }

    private static Category validateAndBuildCategory (String id, String description, String type) {
        long idValue = id!= null && !id.isEmpty() ? Long.parseLong(id) : -1;

        if (type == null || type.isEmpty()) {
            throw new RuntimeException("Type is required!");
        }
        Type typeValue;
        try {
            typeValue = Type.valueOf(type);
        } catch (Exception e) {
            throw new RuntimeException("Invalid type value: '" + type +
                    "', must be one of: " + Arrays.toString(Type.values()));
        }

        return new Category(idValue, description, typeValue);

    }
}
