package wantsome.project.web;

import spark.Request;
import spark.Response;
import wantsome.project.db.dto.Transaction;
import wantsome.project.db.service.CategoryDao;
import wantsome.project.db.service.TransactionDao;

import java.sql.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static wantsome.project.web.SparkUtil.render;

public class AddEditTransactionPageController {

    public static String showAddForm(Request request, Response response) {
        return renderAddUpdateForm("","","","","", null);
    }

    public static String showUpdateForm(Request request, Response response) {
        String id = request.params("id");
        Optional<Transaction> optionalTransaction = TransactionDao.get(Integer.parseInt(id));
        if(!optionalTransaction.isPresent()) {
            throw new RuntimeException("Transaction" + id + "not found!");
        }
        Transaction transaction=optionalTransaction.get();
        return renderAddUpdateForm(
                String.valueOf(transaction.getId()),
                String.valueOf(transaction.getCategoryId()),
                transaction.getDate() != null ? transaction.getDate().toString() : "",
                transaction.getDescription(),
                String.valueOf(transaction.getAmount()),
                "");

    }

    private static String renderAddUpdateForm(String id, String categoryId, String date, String description, String amount,
                                              String errorMsg) {
        Map<String, Object> model = new HashMap<>();

        model.put("isUpdate", id != null && !id.isEmpty());

        model.put("prevId", id);
        model.put("prevCategoryId", categoryId);
        model.put("prevDescription", description);
        model.put("prevAmount", amount);
        model.put("prevDate", date);

        model.put("errorMsg", errorMsg);

        model.put("categoryOptions", CategoryDao.getAll());

        return render("add_edit_transaction.vm", model);

    }


    public static Object handleAddUpdatePostRequest(Request request, Response response) {
        String id = request.queryParams("id");
        String categoryId = request.queryParams("categoryId");
        String date = request.queryParams("date");
        String description = request.queryParams("description");
        String amount = request.queryParams("amount");


        return tryPerformAddUpdateAction(response, id, categoryId, date, description, amount);
    }


    private static Object tryPerformAddUpdateAction(Response response, String id, String categoryId, String date, String description, String amount) {

        try {
           Transaction transaction = validateAndBuildTransaction(id, categoryId, date, description, amount);

           if(transaction.getId() > 0 ){
               TransactionDao.update(transaction);
           } else {
               TransactionDao.insert(transaction);
           }

           response.redirect("/transactions" );
           return response;
        }

        catch (Exception e) {
            return renderAddUpdateForm(id,categoryId,date,description,amount, e.getMessage());
        }
    }

    private static Transaction validateAndBuildTransaction(String id, String categoryId, String date, String description, String amount) {
        long idValue = id != null && !id.isEmpty() ? Long.parseLong(id) : -1;

        long catIdValue = Long.parseLong(categoryId);

        Date dateValue = null;
        if (date != null && !date.isEmpty()) {
            try {
                dateValue = Date.valueOf(date);
            } catch (Exception e) {
                throw new RuntimeException("Invalid due date value: '" + date +
                        "', must be a date in format: yyyy-[m]m-[d]d");
            }
        }

        if(description == null || description.isEmpty()){
            throw new RuntimeException("Description is required");
        }

        float amountValue = Float.parseFloat(amount);
        if (amount == null || amount.isEmpty()) {
            throw new RuntimeException("Amount is required!");
        }

      return new Transaction(idValue, catIdValue, dateValue,description,amountValue);
    }
}



