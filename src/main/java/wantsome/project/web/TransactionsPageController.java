package wantsome.project.web;

import spark.Request;
import spark.Response;
import wantsome.project.db.dto.TransactionFull;
import wantsome.project.db.dto.Type;
import wantsome.project.db.service.TransactionDao;

import java.util.*;

import static java.util.stream.Collectors.toList;
import static wantsome.project.web.SparkUtil.render;

public class TransactionsPageController {

    public enum Order {
        DATE, AMOUNT
    }


    public static String showTransactionsPage (Request req, Response res) {
        List<TransactionFull> allTransactions = TransactionDao.loadAll();

        Type filter = getFilterFromQueryOrSes(req);
        Order order = getOrderFromQueryOrSes(req);
        List<TransactionFull> transactions = getTransactionsToDisplay(allTransactions,filter,order);

        Map<String, Object> model = new HashMap<>();
        model.put("transactions", transactions);
        model.put("crtFilter", filter);
        model.put("crtOrder", order);

        return render("transactions.vm" , model);

    }

    private static Type getFilterFromQueryOrSes(Request request) {
        String param = fromQueryOrSession(request, "filter");
        return param == null ?
                Type.EXPENSE :
                (param.equals("ALL") ?
                        null :
                        Type.valueOf(param));
    }

    private static Order getOrderFromQueryOrSes(Request response) {
        String param = fromQueryOrSession(response, "order");
        return param == null ?
                Order.DATE : //default
                Order.valueOf(param);
    }

    private static String fromQueryOrSession(Request request, String name) {
        String param = request.queryParams(name); //read it from current request (if sent)
        if (param != null) {
            request.session().attribute(name, param); //if found, save it to session for later
        } else {
            param = request.session().attribute(name); //if not found, try to read it from session
        }
        return param;
    }


    private static List<TransactionFull> getTransactionsToDisplay(List<TransactionFull> allTransactions, Type typeFilter, Order order) {
        return allTransactions.stream()
                .filter(n -> typeFilter == null || n.getCategoryType() == typeFilter)
                .sorted(getDisplayComparator(order))
                .collect(toList());
    }


    private static Comparator<TransactionFull> getDisplayComparator(Order order) {
        if (order == Order.AMOUNT) {
            return Comparator.comparing(TransactionFull::getAmount);
        } else {

            Date defaultDate = new Date(0);
            return (i1, i2) -> {
                Date d1 = i1.getDate() != null ? i1.getDate() : defaultDate;
                Date d2 = i2.getDate() != null ? i2.getDate() : defaultDate;
                return d1.compareTo(d2);
            };
        }
    }


    public static Object handleDeleteRequest(Request request, Response response) {
        String id = request.params("id");
        TransactionDao.delete(Long.parseLong(id));
        response.redirect("/transactions");
        return response;
    }


}
