package wantsome.project.web;

import spark.Request;
import spark.Response;

import java.util.HashMap;
import java.util.Map;

import static wantsome.project.web.SparkUtil.render;

public class MainPageController {

    public static String showMainPage(Request request, Response response) {
        Map<String, Object> model = new HashMap<>();

        model.put("transactions", TransactionsPageController.showTransactionsPage(request,response));
        model.put("categories", CategoriesPageController.showCategoriesPage(request,response));


        return render("mainPage.vm", model);

    }
}
