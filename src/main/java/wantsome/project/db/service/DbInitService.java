package wantsome.project.db.service;

import wantsome.project.db.DbManager;
import wantsome.project.db.dto.Category;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import static wantsome.project.db.dto.Type.EXPENSE;
import static wantsome.project.db.dto.Type.INCOME;

public class DbInitService {

    public static void createMissingTables() {

        String CreateCategoriesSql = "create table if not exists categories (\n" +
                "ID integer primary key autoincrement ,\n" +
                "DESCRIPTION text not null unique,\n" +
                "CATEGORY_TYPE text NOT NULL\n" +
                "check(CATEGORY_TYPE in ('" + INCOME + "','" + EXPENSE + "')\n" +
                "))\n";
        String CreateTransactionsSql = "create table if not exists transactions(\n" +
                "ID     integer primary key autoincrement,\n" +
                "CATEGORY_ID long references categories(id) ON DELETE CASCADE, " +
                " DATE_OF_TRANSACTION date not null,\n" +
                "DESCRIPTION text NOT NULL unique,\n" +
                "AMOUNT real not null\n" +
                "check(AMOUNT > 0)" +
                ")";

        try (Connection conn = DbManager.getConnection();
             Statement st = conn.createStatement()) {

            st.execute(CreateCategoriesSql);
            st.execute(CreateTransactionsSql);

        } catch (SQLException e) {
            System.err.println("Error creating missing tables: " + e.getMessage());
        }


    }

    public static void createDefaultExpensesCategories () {
        if(CategoryDao.getAll().isEmpty()) {
            CategoryDao.insert(new Category("Bills", EXPENSE));
            CategoryDao.insert(new Category("Car", EXPENSE));
            CategoryDao.insert(new Category("Clothes", EXPENSE));
            CategoryDao.insert(new Category("Entertainment", EXPENSE));
            CategoryDao.insert(new Category("Food", EXPENSE));
            CategoryDao.insert(new Category("Gifts", EXPENSE));
            CategoryDao.insert(new Category("Health", EXPENSE));
            CategoryDao.insert(new Category("House", EXPENSE));
            CategoryDao.insert(new Category("Pets", EXPENSE));
            CategoryDao.insert(new Category("Sports", EXPENSE));
            CategoryDao.insert(new Category("Toiletry", EXPENSE));
            CategoryDao.insert(new Category("Eating out", EXPENSE));
        }
    }
 }
