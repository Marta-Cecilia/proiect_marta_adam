package wantsome.project.db.service;

import wantsome.project.db.DbManager;
import wantsome.project.db.dto.Category;
import wantsome.project.db.dto.Type;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class CategoryDao {

    public static void insert(Category category) {
        String sql = "INSERT INTO CATEGORIES ( DESCRIPTION, CATEGORY_TYPE )" +
                "VALUES (?,?);";

        try (Connection conn = DbManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {

            ps.setString(1, category.getDescription());
            ps.setString(2, category.getType().toString());

            ps.execute();

        } catch (SQLException e) {
            System.err.println("Error inserting new category in table " + category + " : " + e.getMessage());
        }
    }

    public static List<Category> getAll() {

        String sql = "SELECT * " +
                "FROM CATEGORIES ";

        List<Category> categoryList = new ArrayList<>();

        try (Connection conn = DbManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql);
             ResultSet rs = ps.executeQuery()) {

            while (rs.next()) {
                categoryList.add(extractCategoryFromResultSet(rs));
            }

        } catch (SQLException e) {
            System.err.println("Error loading categories: " + e.getMessage());
        }
        return categoryList;

    }

    public static Optional<Category> get(long id) {
        String sql = "SELECT * FROM CATEGORIES WHERE ID = ?";

        try (Connection conn = DbManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {

            ps.setLong(1, id);
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    Category category = extractCategoryFromResultSet(rs);
                    return Optional.of(category);
                }
            }} catch (SQLException e) {
               throw new RuntimeException("Error loading category with id " + id + ":" + e.getMessage());
            }

            return Optional.empty();

        }


    private static Category extractCategoryFromResultSet(ResultSet rs) throws SQLException {
        long id = rs.getLong("ID");
        String description = rs.getString("DESCRIPTION");
        Type categoryType = Type.valueOf(rs.getString("CATEGORY_TYPE"));


        return new Category(id, description, categoryType);
    }

    public static void update (Category category ){

        String sql = " UPDATE CATEGORIES "+
                "SET DESCRIPTION=?, CATEGORY_TYPE=?" +
                " WHERE ID = ? ";


        try (Connection conn = DbManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {


            ps.setString(1, category.getDescription());
            ps.setString(2, category.getType().name());
            ps.setLong(3, category.getId());

            ps.executeUpdate();

        }catch (SQLException e ){
            System.err.println("Error while updating category " + category + ":" +e.getMessage());
        }
    }

    public static void delete (long id ){

        String sql = " DELETE FROM CATEGORIES WHERE ID = ?";

        try (Connection conn = DbManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {


            ps.setLong(1, id);

            ps.execute();

        }catch (SQLException e ){
            System.err.println("Error while deleting category " + id  + ":" +e.getMessage());
        }
    }
    public static void dropCategoriesTable() {
        String sql = "Drop table categories;";

        try (Connection conn = DbManager.getConnection();
             Statement st = conn.createStatement()) {
            st.execute(sql);
        } catch (SQLException e) {
            System.err.println("Error deleting table categories " + e.getMessage());
        }

    }


}
