package wantsome.project.db.service;

import wantsome.project.db.DbManager;
import wantsome.project.db.dto.Transaction;
import wantsome.project.db.dto.TransactionFull;
import wantsome.project.db.dto.Type;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class TransactionDao {

    public static List<TransactionFull> loadAll() {

        String sql = "SELECT *, CATEGORIES.DESCRIPTION AS Category_Description, CATEGORIES.CATEGORY_TYPE " +
                "FROM TRANSACTIONS " +
                "JOIN CATEGORIES ON TRANSACTIONS.CATEGORY_ID = CATEGORIES.ID " +
                "ORDER BY DATE_OF_TRANSACTION DESC";
        try (Connection conn = DbManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql);
             ResultSet rs = ps.executeQuery()) {

            List<TransactionFull> transactions = new ArrayList<>();
            while (rs.next()) {
                transactions.add(
                        new TransactionFull(
                                rs.getLong("ID"),
                                rs.getLong("CATEGORY_ID"),
                                rs.getDate("DATE_OF_TRANSACTION"),
                                rs.getString("DESCRIPTION"),
                                rs.getFloat("AMOUNT"),
                                rs.getString("CATEGORY_DESCRIPTION"),
                                Type.valueOf(rs.getString("CATEGORY_TYPE"))));
            }
            return transactions;
        } catch (SQLException e) {
            throw new RuntimeException("Error loading transactions: " + e.getMessage());
        }

    }

    public static void insert(Transaction transaction) {
        String sql = "INSERT INTO TRANSACTIONS ( CATEGORY_ID ,DATE_OF_TRANSACTION, DESCRIPTION, AMOUNT) " +
                "VALUES (?,?,?,?)";
        try (Connection conn = DbManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {

            ps.setLong(1, transaction.getCategoryId());
            ps.setDate(2, transaction.getDate());
            ps.setString(3, transaction.getDescription());
            ps.setFloat(4, transaction.getAmount());

            ps.execute();

        } catch (SQLException e) {
            throw new RuntimeException("Error inserting new expense in table " + transaction + ":" + e.getMessage());
        }
    }

    public static List<Transaction> getAll() {

        String sql = "SELECT * " +
                " FROM TRANSACTIONS ";

        List<Transaction> transactionsList = new ArrayList<>();

        try (Connection conn = DbManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql);
             ResultSet rs = ps.executeQuery()) {

            while (rs.next()) {
                transactionsList.add(extractTransactionFromResultSet(rs));
            }

        } catch (SQLException e) {
            System.err.println("Error loading transaction: " + e.getMessage());
        }
        return transactionsList;
    }

    public static Optional<Transaction> get(long id) {
        String sql = "SELECT * FROM TRANSACTIONS WHERE ID=?";

        try (Connection conn = DbManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {

            ps.setLong(1, id);

            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    Transaction transaction = extractTransactionFromResultSet(rs);
                    return Optional.of(transaction);
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException("Error loading transaction with id " + id + " : " + e.getMessage());
        }

        return Optional.empty();
    }

    private static Transaction extractTransactionFromResultSet(ResultSet rs) throws SQLException {

        long id = rs.getLong("ID");
        long categoryId = rs.getLong("CATEGORY_ID");
        Date dateOfTransaction = rs.getDate("DATE_OF_TRANSACTION");
        String description = rs.getString("DESCRIPTION");
        float amount = rs.getFloat("AMOUNT");

        return new Transaction(id, categoryId, dateOfTransaction, description, amount);
    }

    public static void update(Transaction transaction) {

        String sql = "UPDATE TRANSACTIONS " +
                "SET CATEGORY_ID = ? , " +
                "DATE_OF_TRANSACTION = ?," +
                "DESCRIPTION = ? ," +
                "AMOUNT = ? " +
                "WHERE ID = ? ";

        try (Connection conn = DbManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {

            ps.setLong(1, transaction.getCategoryId());
            ps.setDate(2, transaction.getDate());
            ps.setString(3, transaction.getDescription());
            ps.setFloat(4, transaction.getAmount());
            ps.setLong(5, transaction.getId());

            ps.executeUpdate();
        } catch (SQLException e) {
           throw new RuntimeException("Error while updating transaction " + transaction.getId() + e.getMessage());
        }
    }

    public static void delete(long id) {

        String sql = "DELETE FROM TRANSACTIONS WHERE ID = ? ";

        try (Connection conn = DbManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {


            ps.setLong(1, id);

            ps.execute();

        } catch (SQLException e) {
            System.err.println("Error while deleting transaction " + id + ":" + e.getMessage());
        }
    }


    public static void dropTransactionsTable() {
        String sql = "Drop table transactions;";

        try (Connection conn = DbManager.getConnection();
             Statement st = conn.createStatement()) {
            st.execute(sql);
        } catch (SQLException e) {
            System.err.println("Error deleting table transactions " + e.getMessage());
        }

    }
}