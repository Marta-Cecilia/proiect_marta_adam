package wantsome.project.db.dto;

import java.sql.Date;
import java.util.Objects;

public class Transaction {
    private long id;
    private long categoryId;
   private Date date;
    private String description;
    private float amount;

    public Transaction(long id, long categoryId, Date date, String description, float amount) {
        this.id = id;
        this.categoryId = categoryId;
        this.date = date;
        this.description = description;
        this.amount = amount;
    }

    public Transaction(long categoryId, Date date, String description, float amount) {
        this.categoryId = categoryId;
        this.date = date;
        this.description = description;
        this.amount = amount;
    }

    public Transaction(long id, long categoryId, Date date, float amount) {
        this.id = id;
        this.categoryId = categoryId;
        this.date = date;
        this.amount = amount;
    }

    public long getId() {
        return id;
    }

    public long getCategoryId() {
        return categoryId;
    }

    public Date getDate() {
        return date;
    }

    public String getDescription() {
        return description;
    }

    public float getAmount() {
        return amount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Transaction)) return false;
        Transaction that = (Transaction) o;
        return getId() == that.getId() &&
                getCategoryId() == that.getCategoryId() &&
                Float.compare(that.getAmount(), getAmount()) == 0 &&
                Objects.equals(getDate(), that.getDate()) &&
                Objects.equals(getDescription(), that.getDescription());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getCategoryId(), getDate(), getDescription(), getAmount());
    }

    @Override
    public String toString() {
        return "Transactions{" +
                "id=" + id +
                ", categoryId=" + categoryId +
                ", date=" + date +
                ", description='" + description + '\'' +
                ", amount=" + amount +
                '}';
    }
}
