package wantsome.project.db.dto;

import java.sql.Date;
import java.util.Objects;

public class TransactionFull {
    private long id;
    private long categoryId;
    private Date date;
    private String description;
    private float amount;
    private String categoryDescription;
    private Type categoryType;

    public TransactionFull(long id, long categoryId, Date date, String description, float amount, String categoryDescription, Type categoryType) {
        this.id = id;
        this.categoryId = categoryId;
        this.date = date;
        this.description = description;
        this.amount = amount;
        this.categoryDescription = categoryDescription;
        this.categoryType = categoryType;
    }

    public long getId() {
        return id;
    }

    public long getCategoryId() {
        return categoryId;
    }

    public Date getDate() {
        return date;
    }

    public String getDescription() {
        return description;
    }

    public float getAmount() {
        return amount;
    }

    public String getCategoryDescription() {
        return categoryDescription;
    }

    public Type getCategoryType() {
        return categoryType;
    }

    @Override
    public String toString() {
        return "TransactionsFull{" +
                "id=" + id +
                ", categoryId=" + categoryId +
                ", date=" + date +
                ", description='" + description + '\'' +
                ", amount=" + amount +
                ", categoryDescription='" + categoryDescription + '\'' +
                ", categoryType=" + categoryType +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TransactionFull)) return false;
        TransactionFull that = (TransactionFull) o;
        return getId() == that.getId() &&
                getCategoryId() == that.getCategoryId() &&
                Float.compare(that.getAmount(), getAmount()) == 0 &&
                Objects.equals(getDate(), that.getDate()) &&
                Objects.equals(getDescription(), that.getDescription()) &&
                Objects.equals(getCategoryDescription(), that.getCategoryDescription()) &&
                getCategoryType() == that.getCategoryType();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getCategoryId(), getDate(), getDescription(), getAmount(), getCategoryDescription(), getCategoryType());
    }
}
