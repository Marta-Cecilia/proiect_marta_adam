package wantsome.project.db.dto;

import java.util.Objects;

public class Category {

    private long id;
    private String description;
    private Type type;

    public Category(long id, String description, Type type) {
        this.id = id;
        this.description = description;
        this.type = type;
    }

    public Category(String description, Type type) {

        this.description = description;
        this.type = type;
    }

    public long getId() {
        return id;
    }

    public Type getType() {
        return type;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Category)) return false;
        Category that = (Category) o;
        return getId() == that.getId() &&
                Objects.equals(getDescription(), that.getDescription()) &&
                getType() == that.getType();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getDescription(), getType());
    }

    @Override
    public String toString() {
        return "Categories{" +
                "id=" + id +
                ", description='" + description + '\'' +
                ", type=" + type +
                '}';
    }

}
